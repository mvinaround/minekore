<?php include("webkore_files/osc_core.php");
	LoadHeader();
	CheckForUser();

	$alert_box = false;
	$alert_text = "";
	$alert_type = "";


	if(isset($_FILES['image'])){
      $errors= array();
      $file_name = $_FILES['image']['name'];
      $file_size =$_FILES['image']['size'];
      $file_tmp =$_FILES['image']['tmp_name'];
      $file_type=$_FILES['image']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
      
      $expensions= array("gif","jpeg","jpg","png");
      
      if(in_array($file_ext,$expensions)=== false){
         $errors[]="Please choose a JPEG, PNG or GIF file.";
         $alert_text = $alert_text . " Please choose a JPEG, PNG or GIF file.";
      }
      
      
      if(empty($errors)==true){
         move_uploaded_file($file_tmp,"images/".$file_name);
      }else{
         $alert_box = true;
         $alert_type = "danger";
      }
   }

?>
<section class="popout">
	<h1>Images</h1>
	<p>Here you can manage the images and get links to insert them into the website editor or even to share with customers or on social media. <strong>Be Careful</strong> when deleting images as you may have used them elsewhere on your website!</p>

	<p><strong>Note:</strong> This currently doesn't work with Safari browsers. Please use something a bit more useful and modern ;)</p>

	<p>	
	<?php if($_GET["mode"] == "delete"){ ?>	
	<a href="image_uploads.php" class="btn btn-add">Get Link Mode</a>

	<?php }
		else{
	?>
	
	<a href="image_uploads.php?mode=delete" class="btn btn-delete">Delete Mode</a>

	<?php } ?>

	</p>

	<?php


		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>
	<div class="images">
		<?php

			$files = glob('images/*.{gif,jpg,png,jpeg,JPEG,JPG,GIF}', GLOB_BRACE);
			foreach($files as $file) {
					$image_id = rand(1,12345);
					if($_GET["mode"] == "delete"){
				?>
						<div class="g-image" id="image-<?php echo $image_id ?>" data-fs-url="<?php echo $file ?>" style="background-image: url('<?php echo $file ?>')">
							<a onclick="ImageDelete(<?php echo $image_id; ?>)">Delete</a>
						</div>

				<?php
					}
					else{
						?>
							<div class="g-image" id="image-<?php echo $image_id ?>" data-fs-url="<?php echo $file ?>" style="background-image: url('<?php echo $file ?>')">
							<a onclick="ImageGetLink(<?php echo $image_id; ?>)">Get Link</a>
							</div>

						<?php
					}
			}

		?>
	</div>

	<div class="add-image">
	<h2>Upload A New Image</h2>
		<form action="" method="POST" enctype="multipart/form-data">
		<div class="form-field">
			<label for="file">Select An Image</label>
			<input type="file" name="image"  accept="image/*"/>
			<input type="submit" value="Upload"/>
		</div>

      </form>
	</div>


</section>

<div class="textbox-dropdown tb-dd-hide">
	<h2>Your Image Link:</h2>
	<input type="text" id="copy-link-box">
	<br>
	<br>
	<a class="btn btn-add" onclick="GotLink();">Got It!</a>
</div>
