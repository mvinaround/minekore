<?php include("webkore_files/osc_core.php");
	LoadHeader();
	CheckForUserLoginScreen();

	if($_GET["action"] == "logout"){
		LogAction("User Logged Out");	
		session_destroy();
		$alert_box = true;
		$alert_type = "success";
		$alert_text = "Logged Out.";

	}

	$email = $_POST["email"];
	$password = $_POST["password"];

	
?>
<div class="osc-logo"></div>
<div class="login-screen">
	<h2>Please Log In</h2>
	<p class="dev-msg alert info"><strong>Please Note:</strong> WebKore CMS Mobile is in Beta Stage. This means its still in development. Please let me know if you have any problems :)</p>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}

	if($email){
		if(CheckLogin($email, $password)){
			echo '<div class="alert error">Incorrect Password/Email Address.</div>';
		}
	}

	?>
	<form method="post" action="index.php">
		<div class="form-field">
			<label for="email">Email:</label>
			<input type="email" name="email" value="<?php echo $email; ?>" requried>
		</div>
		<div class="form-field">
			<label for="password">Password:</label>
			<input type="password" name="password" requried>
		</div>
		<div class="form-field">
			<input type="submit" value="Log In" requried>
		</div>
	</form>
	<br>
	<p>Forgot your password? <a href="forgot-password.php?view=reset">Reset it here</a></p>
	<br>
	<p>Created & Maintained By: <br>Jamie Overington & JJMedia UK &copy; 2017</p>
</div>



