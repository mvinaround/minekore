<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	
	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

	//On adding a new one:
	if($_POST["action"] == "doadd"){
		$name = $_POST["name"];
		$content = $_POST["content"];

		if(SQLQuery("INSERT INTO contents (name,content) VALUES ('" . $name . "','" .  SQLSafe($content) . "')" )){
			$alert_box = true;
			$alert_text = "Added New Content Page!";
			$alert_type = "success";

			LogAction("Created new Content: [" . $name . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Save Content Page.";
			$alert_type = "danger";
		}

	}
	//On updating an existing one:

	elseif($_POST["action"] == "doedit"){
		if(SQLQuery("UPDATE contents SET name = '" . $_POST["name"] .  "', content = '" .  SQLSafe($_POST["content"]) . "' WHERE id = ". $_POST["id"])){
			$alert_box = true;
			$alert_text = "Updated ". $_POST["name"] . " Content Page!";
			$alert_type = "success";	
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Update Content Page.";
			$alert_type = "danger";
		}
	}

	else{
		$action = $_GET["action"];	
	}
?>
<section>


	<h1>Content Pages</h1>
	<p>Content pages are a full page as shown on your website. Here you can edit them and update your site.</p>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>

	<?php 
		if($action == "add" or $action == "edit"){
			$name = "";
			$description = "";
			$content = "";
			$send_action = "doadd";
			$id = 0;
			$button_text = "Create";

			if($action == "edit"){
				$id = $_GET["id"];
				$query = mysqli_fetch_array(SQLQuery("SELECT * FROM contents WHERE id = " . intval($id) . " LIMIT 1"));
				$name = $query["name"];
				$content = $query["content"];
				$send_action = "doedit";
				$button_text = "Update";	

			}


	?>

	<div class="add">
		<form method="post">
			<div class="form-field">
				<label for="name">Name</label>
				<input type="text" name="name" value="<?php echo $name;?>" required/>
			</div>
			<div class="form-field">
				<label for="content">Content</label>
				<textarea id="htmleditor" name="content"><?php echo $content;?></textarea>
				<?php MakeEditor("#htmleditor",1000); ?>
			</div>
			<input type="hidden" name="action" value="<?php echo $send_action;?>" />
			<input type="hidden" name="id" value="<?php echo $id;?>" />
			<div class="form-field">
				<input type="submit" value="<?php echo $button_text ?> Content Page" />
			</div>
		</form>
	</div>


	<?php
		}
		if($action == ""){

	?>
	<div class="list">
		<a href="contents.php?action=add" class="btn btn-add"><i class="fa fa-plus"></i>  Create New Content Page</a>
		<table>
			<tr><th>Page Name</th><th>Actions</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM contents");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr id="contents-<?php echo $row['id'] ?>" >
			    			<td><?php echo $row['name'] ?></td>
			    			<td class="table-actions">
			    				<a class="btn btn-add" href="contents.php?action=edit&id=<?php echo $row["id"] ?>">Edit</a>
			    				<a class="btn btn-delete" onclick="DBDelete(<?php echo $row['id'] ?>,'contents')">Delete</a>

			    			</td>
						</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Content Pages Found.</p></td></tr>";
				}
		?>
		</table>
	</div>

	<?php } ?>
</section>


<?php LoadFooter(); ?>
