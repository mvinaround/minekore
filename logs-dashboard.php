<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();
	CheckForUser();
	
?>
<section>
	<h1>System Logs</h1>
	<br><br>
	<div class="quick-buttons">
		<a href="logs-emails.php" class="dash-button">
			<i class="fa fa-envelope"></i>
			<p>Emails</p>
		</a>
		<a href="logs-users.php" class="dash-button">
			<i class="fa fa-users"></i>
			<p>Users</p>
		</a>
	</div>
</section>


<?php LoadFooter(); ?>
