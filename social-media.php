<?php include("webkore_files/osc_core.php");
	CheckForUser();
	LoadHeader();
	LoadMenu();
	

	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

	if($_POST["action"] == "update_social_urls"){

		UpdateSetting("facebook_url");


		$alert_box = true;
		$alert_text = "Updated Social Media URLs";
		$alert_type = "success";

		LogAction("Updated Social Media URLs");


	}

	if($_POST["action"] == "update_facebook_like"){

		UpdateSetting("fb_like_layout");
		UpdateSetting("fb_like_action_type");
		UpdateSetting("fb_like_button_size");
		UpdateSetting("fb_like_button_colour");

		$alert_box = true;
		$alert_text = "Updated Facebook Like Settings.";
		$alert_type = "success";

		LogAction("Updated Facebook Like Settings");


	}

	if($_POST["action"] == "update_facebook_contact"){

		UpdateSetting("fb_enable_message_box_on_contact_page");
		UpdateSetting("fb_direct_message_hide_cover");

		$alert_box = true;
		$alert_text = "Updated Facebook Direct Message Settings.";
		$alert_type = "success";

		LogAction("Updated Facebook Direct Message Settings.");


	}



	
?>
<section>
	<h1>Social Media Settings</h1>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>
	<p>Listed Here are all the settings used across the site. Please be careful when changing these settings as they can effect your site performace or break some things. For help please email <a href="mailto:contact@jjmediauk.co.uk?subject=Advanced Settings Help">contact@jjmediauk.co.uk</a> for more help.</p>
	<h2>Basic Settings</h2>
		<form for="social-media.php" method="POST">
		<?php
			FormField( "Facebook URL", "text", 0, "get-setting","URL Or Facebook Page to link Like button to.");
		?>
			<div class="form-field">
				<input type="hidden" name="action" value="update_social_urls" />
				<input type="submit" value="Update Social Media URLs" />
			</div>
		</form>

	<div class="advanced-settings">
		<h2>Facebook Settings</h2>
		<p>Enable and tweak Facebook Link settings here. Please Toggle feature on a refresh the page.</p>

		<h3 class="option-header">Site Wide Facebook Like/Share Buttons</h3>
		<?php SwitchFeature("social-facebook-like-buttons");?>

		<h3 class="option-header">Message Facebook Page on Contact Page</h3>
		<?php SwitchFeature("social-facebook-contact-form");?>


		<?php if(GetFeature(1,0,"social-facebook-like-buttons")){ ?>
		<br><br>

		<h2 class="section-toggle">Customizations For Like Button <i id="fb_like-toggle" class="toggle fa fa-chevron-circle-up"></i></h2>
		<div class="collapsible" id="fb_like" data-hidden="false">
			<div id="fb-root"></div>
				<form for="social-media.php" method="POST">
					<div class="form-field">
						<label>Button Layout (See Examples Below)</label>
						<label><strong>BETA:</strong> Currently set as <strong><?php echo GetSetting("fb_like_layout")?></strong></label>
						<select id="fb_like_layout" name="fb_like_layout" onchange="GenerateFacebookLikePreview()">
		  					<option value="standard">Standard</option>
		  					<option value="box_count">Box With Counter</option>
		 					<option value="button_count">Counter In Like Button</option>
		  					<option value="button">Plain Buttons</option>
						</select>
					</div>

					<div class="form-field">
						<label>Choose Button Text</label>
						<label><strong>BETA:</strong> Currently set as <strong><?php echo GetSetting("fb_like_action_type")?></strong></label>
						<select id="fb_like_action_type" name="fb_like_action_type" onchange="GenerateFacebookLikePreview()">
		  					<option value="like">Like</option>
		  					<option value="recommend">Recommend</option>
						</select>
					</div>

					<div class="form-field">
						<label>Button Size</label>
						<label><strong>BETA:</strong> Currently set as <strong><?php echo GetSetting("fb_like_button_size")?></strong></label>
						<select id="fb_like_button_size" name="fb_like_button_size" onchange="GenerateFacebookLikePreview()">
		  					<option value="small">Small</option>
		  					<option value="large">Regular</option>
						</select>
					</div>
					<div class="form-field">
						<label>Button Size</label>
						<label><strong>BETA:</strong> Currently set as <strong><?php echo GetSetting("fb_like_button_colour")?></strong></label>
						<select id="fb_like_button_colour" name="fb_like_button_colour" onchange="GenerateFacebookLikePreview()">
		  					<option value="light">Light</option>
		  					<option value="dark">Dark</option>
						</select>
					</div>
					<input type="hidden" id="fb_page_url" name="fb_page_url" value="<?php echo urlencode(GetSetting("facebook_url")) ?>" />
					<p>Preview:</p>
					<div id="fb-like-preview"><iframe src="<?php echo 'https://www.facebook.com/v2.7/plugins/like.php?action=' . GetSetting("fb_like_action_type") . '&app_id=181687705935449&channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FlY4eZXm_YWu.js%3Fversion%3D42%23cb%3Df8f2445c5a8814%26domain%3Dlocalhost%26origin%3Dhttp%253A%252F%252Flocalhost%252Ff112aed3676c6d8%26relation%3Dparent.parent&container_width=1904&href=' . urlencode(GetSetting("facebook_url")) . '&colorscheme=' . GetSetting("fb_like_button_colour") . '&layout=' . GetSetting("fb_like_layout") . '&locale=en_GB&sdk=joey&share=true&show_faces=true&size=' . GetSetting("fb_like_button_size");?>"></iframe></div>

					<div class="form-field">
					<input type="hidden" name="action" value="update_facebook_like" />
						<input type="submit" value="Update Facebook Like Button Options" />	
					</div>
				</form>
			</div>

		<?php } ?>

		<?php if(GetFeature(1,0,"social-facebook-contact-form")){ ?>
			<h2 class="section-toggle">Customizations For Contact Page Message Box<i id="fb_contact-toggle" class="toggle fa fa-chevron-circle-up"></i></h2>
			<div class="collapsible" id="fb_contact" data-hidden="false">
				<form for="social-media.php" method="POST">
					<?php FormField("FB Enable Message Box On Contact Page","checkbox",0,"get-setting","Display message box on contact page?");
						FormField("FB Direct Message Hide Cover","checkbox",0,"get-setting","Hide Cover Image at the top of the box?");?>



					<div class="form-field">
						<input type="hidden" name="action" value="update_facebook_contact" />
						<input type="submit" value="Update Facebook Contact Options" />	
					</div>

				</form>

			</div>


		<?php } ?>


	</div>

	
</section>
<script>

var APIKey = "<?php echo $enc_key ?>"

$(".onoffswitch-checkbox").change(function() {
	var val = 0;
	var name = this.id.replace("f_","")

    if(this.checked) {
    	val = 1
    }

    FeatureToggle(name,val,APIKey);
});


</script>

<?php LoadFooter(); ?>
