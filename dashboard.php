<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();
	CheckForUser();
	
?>
<section>
	<h1>Dashboard</h1>
	<h2>Welcome Back <?php echo GetUsername() ?></h2>
	<br><br>
	<div class="quick-buttons">
		<?php if(GetFeature(1,0,"ql-snippets")){ ?>
			<a href="snippets.php" class="dash-button">
				<i class="fa fa-code"></i>
				<p>Snippets</p>
			</a>
		<?php } ?>

		<?php if(GetFeature(1,0,"ql-contents")){ ?>
		<a href="contents.php" class="dash-button">
			<i class="fa fa-file-text"></i>
			<p>Pages</p>
		</a>
		<?php } ?>

		<?php if(GetFeature(1,0,"ql-gallery")){ ?>
			<?php if(GetFeature(1,0,"gallery")){ ?>
				<a href="gallery.php" class="dash-button">
					<i class="fa fa-picture-o"></i>
					<p>Gallery</p>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(GetFeature(1,0,"ql-banners")){ ?>
			<?php if(GetFeature(1,0,"banners")){ ?>
				<a href="banners.php" class="dash-button">
					<i class="fa fa-picture-o"></i>
					<p>Banners</p>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(GetFeature(1,0,"ql-updates")){ ?>
			<a href="updates.php" class="dash-button">
				<i class="fa fa-thumbs-up"></i>
				<p>Updates</p>
			</a>
		<?php } ?>
	</div>
	<?php if(GetFeature(1,0,"ql-visits")){ ?>
	<?php } ?>
</section>


<?php LoadFooter(); ?>
