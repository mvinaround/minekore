<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	
	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

	//On adding a new one:
	if($_POST["action"] == "doadd"){
		$name = $_POST["name"];
		$description = $_POST["description"];
		$serial = $_POST["serial"];
		$category = ucwords($_POST["category"]);
		$owner = $_POST["owner"];
		$worth = $_POST["worth"];

		if(SQLQuery("INSERT INTO inventory (name,description,serial,category,owner,worth) VALUES ('" . $name . "','" . $description . "','" . $serial . "','" . $category . "','" . $owner . "','" . $worth . "')" )){
			$alert_box = true;
			$alert_text = "Added New Item to Inventory!";
			$alert_type = "success";

			LogAction("Created a new Inventory Item: [" . $name . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Save Inventory Item.";
			$alert_type = "danger";
		}

	}
	//On updating an existing one:

	elseif($_POST["action"] == "doedit"){
		if(SQLQuery("UPDATE inventory SET name = '" . $_POST["name"] . "', description = '" . $_POST["description"] . "', serial = '" . $_POST["serial"] . "' , category ='" . $_POST["category"] . "', owner = '" . $_POST["owner"] . "', worth = '" . $_POST["worth"] . "' WHERE id = ". $_POST["id"])){
			$alert_box = true;
			$alert_text = "Updated ". $_POST["name"] . " Inventory Item!";
			$alert_type = "success";

			LogAction("Updated Inventory Item #" . $_POST["id"] . ": [" . $_POST["name"] . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Update Inventory Item: " . $_POST["id"];
			$alert_type = "danger";
		}
	}

	else{
		$action = $_GET["action"];	
	}
?>
<section>


	<h1>Inventory Items</h1>
	<p>Use this section to safely organise and store the serials etc of kit or items that you own.</p>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>

	<?php 
		if($action == "add" or $action == "edit"){
			$name = "";
			$description = "";
			$content = "";
			$send_action = "doadd";
			$id = 0;
			$button_text = "Create";

			if($action == "edit"){
				$id = $_GET["id"];
				$query = mysqli_fetch_array(SQLQuery("SELECT * FROM inventory WHERE id = " . intval($id) . " LIMIT 1"));
				$name = $query["name"];
				$description = $query["description"];
				$serial = $query["serial"];
				$category = ucwords($query["category"]);
				$owner = $query["owner"];
				$worth = $query["worth"];
				$send_action = "doedit";
				$button_text = "Update";	

			}


	?>

	<div class="add">
		<form method="post">
			<div class="form-field">
				<label for="name">Name</label>
				<input type="text" name="name" value="<?php echo $name;?>" required/>
			</div>
			<div class="form-field">
				<label for="description">Description</label>
				<textarea name="description" ><?php echo $description;?></textarea>
			</div>
			<div class="form-field">
				<label for="serial">Serial Number</label>
				<input type="text" name="serial" value="<?php echo $serial;?>"/>
			</div>
			<div class="form-field">
				<label for="category">Category</label>
				<input type="text" name="category" value="<?php echo $category;?>"/>
			</div>
			<div class="form-field">
				<label for="owner">Owner</label>
				<input type="text" name="owner" value="<?php echo $owner;?>"/>
			</div>
			<div class="form-field">
				<label for="worth">Worth (How much to replace)</label>
				<input type="number" min="0.00" step="0.01" name="worth" value="<?php echo $worth;?>"/>
			</div>

			<input type="hidden" name="action" value="<?php echo $send_action;?>" />
			<input type="hidden" name="id" value="<?php echo $id;?>" />
			<div class="form-field">
				<input type="submit" value="<?php echo $button_text ?> Inventory Item" />
			</div>
		</form>
	</div>


	<?php
		}
		if($action == ""){

	?>
	<div class="list">
		<a href="inventory.php?action=add" class="btn btn-add"><i class="fa fa-plus"></i>  Create New Item</a>
		<table>
			<tr><th>Item Name</th><th>Serial</th><th>Worth</th><th>Owner</th><th>Category</th><th>Actions</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM inventory ORDER BY category, name DESC");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr id="inventory-<?php echo $row['id'] ?>" >
			    			<td><?php echo $row['name'] ?></td>
			    			<td><?php echo $row['serial'] ?></td>
			    			<td>&pound;<?php echo $row['worth'] ?></td>
			    			<td><?php echo $row['owner'] ?></td>
			    			<td><?php echo $row['category'] ?></td>
			    			<td class="table-actions">
			    				<a class="btn btn-add" href="inventory.php?action=edit&id=<?php echo $row["id"] ?>">Edit</a>
			    				<a class="btn btn-delete" onclick="DBDelete(<?php echo $row['id'] ?>,'inventory')">Delete</a>

			    			</td>
			    		</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Items Found.</p></td></tr>";
				}
		?>
		</table>
	</div>

	<?php } ?>
</section>


<?php LoadFooter(); ?>
