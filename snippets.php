<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	
	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

	//On adding a new one:
	if($_POST["action"] == "doadd"){
		$name = $_POST["name"];
		$desc = $_POST["desc"];
		$content = $_POST["content"];

		if(SQLQuery("INSERT INTO snippets (name,description,content) VALUES ('" . $name . "','" . $desc . "','" .  SQLSafe($content) . "')" )){
			$alert_box = true;
			$alert_text = "Added New Snippet!";
			$alert_type = "success";

			LogAction("Created a new snippet: [" . $name . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Save Snippet.";
			$alert_type = "danger";
		}

	}
	//On updating an existing one:

	elseif($_POST["action"] == "doedit"){
		if(SQLQuery("UPDATE snippets SET name = '" . $_POST["name"] . "', description = '" . $_POST["desc"] . "', content = '" .  SQLSafe($_POST["content"]) . "' WHERE id = ". $_POST["id"])){
			$alert_box = true;
			$alert_text = "Updated ". $_POST["name"] . " Snippet!";
			$alert_type = "success";

			LogAction("Updated snippet #" . $_POST["id"] . ": [" . $_POST["name"] . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Update Snippet.";
			$alert_type = "danger";
		}
	}

	else{
		$action = $_GET["action"];	
	}
?>
<section>


	<h1>Snippets</h1>
	<p>Snippets are parts of your website that are not complete pages, but are small pieces of information or text. Snippets are placed in the hard coded parts of certian pages and are edited here.</p>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>

	<?php 
		if($action == "add" or $action == "edit"){
			$name = "";
			$description = "";
			$content = "";
			$send_action = "doadd";
			$id = 0;
			$button_text = "Create";

			if($action == "edit"){
				$id = $_GET["id"];
				$query = mysqli_fetch_array(SQLQuery("SELECT * FROM snippets WHERE id = " . intval($id) . " LIMIT 1"));
				$name = $query["name"];
				$description = $query["description"];
				$content = $query["content"];
				$send_action = "doedit";
				$button_text = "Update";	

			}


	?>

	<div class="add">
		<form method="post">
			<div class="form-field">
				<label for="name">Name</label>
				<input type="text" name="name" value="<?php echo $name;?>" required/>
			</div>
			<div class="form-field">
				<label for="desc">Description</label>
				<textarea name="desc" required><?php echo $description;?></textarea>
			</div>
			<div class="form-field">
				<label for="content">Content</label>
				<textarea id="htmleditor" name="content"><?php echo $content;?></textarea>
				<?php MakeEditor("#htmleditor",500); ?>
			</div>
			<input type="hidden" name="action" value="<?php echo $send_action;?>" />
			<input type="hidden" name="id" value="<?php echo $id;?>" />
			<div class="form-field">
				<input type="submit" value="<?php echo $button_text ?> Snippet" />
			</div>
		</form>
	</div>


	<?php
		}
		if($action == ""){

	?>
	<div class="list">
		<a href="snippets.php?action=add" class="btn btn-add"><i class="fa fa-plus"></i>  Create New Snippet</a>
		<table>
			<tr><th>Snippet Name</th><th>Description</th><th>Actions</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM snippets");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr id="snippets-<?php echo $row['id'] ?>" >
			    			<td><?php echo $row['name'] ?></td>
			    			<td><?php echo $row['description'] ?></td>
			    			<td class="table-actions">
			    				<a class="btn btn-add" href="snippets.php?action=edit&id=<?php echo $row["id"] ?>">Edit</a>
			    				<a class="btn btn-delete" onclick="DBDelete(<?php echo $row['id'] ?>,'snippets')">Delete</a>

			    			</td>
			    		</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Snippets Found.</p></td></tr>";
				}
		?>
		</table>
	</div>

	<?php } ?>
</section>


<?php LoadFooter(); ?>
