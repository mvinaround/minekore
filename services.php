<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	
	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

	//On adding a new one:
	if($_POST["action"] == "doadd"){
		$title = $_POST["title"];
		$desc = $_POST["desc"];
		$image = $_POST["image"];

		if(SQLQuery("INSERT INTO services (title,description,image) VALUES ('" . SQLSafe($title) . "','" . SQLSafe($desc) . "','" .  SQLSafe($image) . "')" )){
			$alert_box = true;
			$alert_text = "Added New Service!";
			$alert_type = "success";

			LogAction("Created a new Service: [" . $title . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Save Service.";
			$alert_type = "danger";
		}

	}
	//On updating an existing one:

	elseif($_POST["action"] == "doedit"){
		if(SQLQuery("UPDATE services SET title = '" . SQLSafe($_POST["title"]) . "', description = '" . SQLSafe($_POST["desc"]) . "', image = '" .  SQLSafe($_POST["image"]) . "' WHERE id = ". $_POST["id"])){
			$alert_box = true;
			$alert_text = "Updated ". $_POST["name"] . " Service!";
			$alert_type = "success";

			LogAction("Updated Service #" . $_POST["id"] . ": [" . $_POST["title"] . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Update Service.";
			$alert_type = "danger";
		}
	}

	else{
		$action = $_GET["action"];	
	}
?>
<section>


	<h1>Services</h1>
	<p>Here you can edit and add new services. These will be displayed on your site.</p>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>

	<?php 
		if($action == "add" or $action == "edit"){
			$title = "";
			$description = "";
			$image = "";
			$send_action = "doadd";
			$id = 0;
			$button_text = "Create";

			if($action == "edit"){
				$id = $_GET["id"];
				$query = mysqli_fetch_array(SQLQuery("SELECT * FROM services WHERE id = " . intval($id) . " LIMIT 1"));
				$title = $query["title"];
				$description = $query["description"];
				$image = $query["image"];
				$send_action = "doedit";
				$button_text = "Update";	

			}


	?>

	<div class="add">
		<form method="post">
			<div class="form-field">
				<label for="title">Title</label>
				<input type="text" name="title" value="<?php echo $title;?>" required/>
			</div>
			<div class="form-field">
				<label for="desc">Description</label>
				<textarea name="desc" required><?php echo $description;?></textarea>
			</div>
			<div class="form-field">
				<label for="image">Image URL</label>
				<input type="text" name="image" value="<?php echo $image;?>" />
			</div>
			<div class="form-field">
				<br><a class="btn btn-info" onclick="ImageManager();">Manage Images (Popup window)</a><br><br>
			</div>
			<input type="hidden" name="action" value="<?php echo $send_action;?>" />
			<input type="hidden" name="id" value="<?php echo $id;?>" />
			<div class="form-field">
				<input type="submit" value="<?php echo $button_text ?> Service" />
			</div>
		</form>
	</div>


	<?php
		}
		if($action == ""){

	?>
	<div class="list">
		<a href="services.php?action=add" class="btn btn-add"><i class="fa fa-plus"></i>  Create New Service</a>
		<table>
			<tr><th>Service Name</th><th>Description</th><th>Actions</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM services");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr id="services-<?php echo $row['id'] ?>" >
			    			<td><?php echo $row['title'] ?></td>
			    			<td><?php echo $row['description'] ?></td>
			    			<td class="table-actions">
			    				<a class="btn btn-add" href="services.php?action=edit&id=<?php echo $row["id"] ?>">Edit</a>
			    				<a class="btn btn-delete" onclick="DBDelete(<?php echo $row['id'] ?>,'services')">Delete</a>

			    			</td>
			    		</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Services Found.</p></td></tr>";
				}
		?>
		</table>
	</div>

	<?php } ?>
</section>


<?php LoadFooter(); ?>
