<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();
	CheckForUser();

	$date_from = $_GET["from"];
	$date_to = $_GET["to"];
	$today = date("Y-m-d");
	$bots_included = "bot = 0 AND";
	$bots = "";

	if($_GET["bots"] == "on"){
		$bots_included = "";
		$bots = "checked";
	}

	$default_from = date('Y-m-d', strtotime(date() . '-1 month'));
	$default_to = date('Y-m-d', strtotime(date() . '+1 day'));

	if($date_from == ""){
		$date_from = $default_from;
	}
	if($date_to == ""){
		$date_to = $default_to;
	}
	$data = SQLQuery("SELECT * FROM visits WHERE " . $bots_included . " created_at >= '" . $date_from . " 00:00:00' AND created_at <='" . $date_to . " 23:59:59' ORDER BY created_at ASC");

	$unique_visitors = mysqli_num_rows(SQLQuery("SELECT DISTINCT ip FROM visits WHERE " . $bots_included . " created_at >= '" . $date_from . " 00:00:00' AND created_at <='" . $date_to . " 23:59:59'"));
	$unique_views = mysqli_num_rows(SQLQuery("SELECT id FROM visits WHERE " . $bots_included . " created_at >= '" . $date_from . " 00:00:00' AND created_at <='" . $date_to . " 23:59:59'"));
	$unique_pages = mysqli_num_rows(SQLQuery("SELECT DISTINCT ip,request_uri FROM visits WHERE " . $bots_included . " created_at >= '" . $date_from . " 00:00:00' AND created_at <='" . $date_to . " 23:59:59'"));
	$unique_browsers = mysqli_num_rows(SQLQuery("SELECT DISTINCT user_agent FROM visits WHERE " . $bots_included . " created_at >= '" . $date_from . " 00:00:00' AND created_at <='" . $date_to . " 23:59:59'"));
	
	//Old Data Clean-up:
	SQLQuery("DELETE FROM visits WHERE created_at < '" . date('Y-m-d', strtotime(date() . '-3 months')) . "'")


?>
<script>
	google.charts.load('current', {'packages':['corechart','map','geochart']});
</script>
<script src="osc_files/lib/markerclusterer.js"></script>
<section>
	<h1>Site Performance</h1>

	<h2>Site Visits</h2>
	<div class="alert warning">The Site Visitor data is currently undergoing a huge re-build, therefore any current issues will be ignored until the next update.</div>
	<p>The data shown here is only from up to the last 3 months. To change the data being shown, use the settings below...</p>
	<form>
		<div class="inline-date-field">
			<label for="from">Date From:</label>
			<input type="date" name="from" value= "<?php echo $date_from ?>"/>
			<label for="to">Date To:</label>
			<input type="date" name="to" value= "<?php echo $date_to ?>"/>
			<label for="bots" <?php $bots ?> >Include Bot Data?</label>
			<input type="checkbox" name="bots"<?php echo $bots ?> />
			<input type="submit" value="Search" class="btn btn-add" />
			<button href="site-performance.php" class="btn btn-delete">Clear</button>
		</div>
	</form>
	<h2>Quick Stats</h2>
	<div class="quick-data-square">
		<p class="text">Unique Visitors</p>
		<p class="value"><?php echo $unique_visitors ?></p>
	</div>
	<div class="quick-data-square">
		<p class="text">Unique Views</p>
		<p class="value"><?php echo $unique_views ?></p>
	</div>
	<div class="quick-data-square">
		<p class="text">Unique Page Views</p>
		<p class="value"><?php echo $unique_pages ?></p>
	</div>
	<div class="quick-data-square">
		<p class="text">Different Browsers</p>
		<p class="value"><?php echo $unique_browsers ?></p>
	</div>

	<h2> Visits Per Day</h2>
	<div class="chart" id="line-graph">
		<script>
		
		google.charts.setOnLoadCallback(drawChart);

		function drawChart() {
			var data = google.visualization.arrayToDataTable([
				['Day', 'Views', 'Unique Visitors']
				<?php
					$current_date = "";
					$current_date_raw = "";
					$last_date = "";
					$last_date_raw = "";
					$views = 0;

					while($row = mysqli_fetch_array($data)){
						$current_date =  strtotime($row['created_at']);
						$current_date = $current_date;
						$current_date_raw = $current_date;
						$current_date = date("d-m-y", $current_date);

						if($last_date == ""){
							$last_date = $current_date;
							$last_date_raw = $current_date_raw;
						}

						if($current_date == $last_date){
							$views += 1;
							$last_date_raw = $current_date_raw;

						}
						else{
							$unique_visitors = mysqli_num_rows(SQLQuery("SELECT DISTINCT ip FROM visits WHERE created_at BETWEEN '" . date("Y-m-d", $last_date_raw) . " 00:00:00' AND '" . date("Y-m-d", $last_date_raw) . " 23:59:59'"));

							echo ",['";
							echo date("D d M y", $last_date_raw);
							echo "',";
							echo $views;
							echo ",";
							echo $unique_visitors;
							echo "]";
							$views = 1;
							$last_date = $current_date;
						}
					}
					//Write last date then...

							$unique_visitors = mysqli_num_rows(SQLQuery("SELECT DISTINCT ip FROM visits WHERE created_at BETWEEN '" . date("Y-m-d", $last_date_raw) . " 00:00:00' AND '" . date("Y-m-d", $last_date_raw) . " 23:59:59'"));

							echo ",['";
							echo date("D d M y", $last_date_raw);
							echo "',";
							echo $views;
							echo ",";
							echo $unique_visitors;
							echo "]";

					mysqli_data_seek($data, 0);
				?>
			]);

			var options = {
				title: 'Views Per Day Within <?php echo MakeUKDate($date_from) . " - " . MakeUKDate($date_to) ?>',
				crosshair: { trigger: 'both' },
				vAxis: {minValue: 0},
				legend: { position: 'right' },
				tooltip: { isHtml: true }
			};

			function resize() {
				var chart = new google.visualization.AreaChart(document.getElementById('line-graph'));
				chart.draw(data, options);
			}

			window.onload = resize();
			window.onresize = resize;
		}	
		</script>
	</div>

	<h2>Geo Location Map</h2>
	<div class="chart" id="geo-map">

	<script>

      function initMap() {

        var map = new google.maps.Map(document.getElementById('geo-map'), {
          zoom: 3,
          center: {lat: 55.3781, lng: -2.610892}
        });

        // Create an array of alphabetical characters used to label the markers.
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
        var markers = locations.map(function(location, i) {
          return new google.maps.Marker({
            position: location,
            label: labels[i % labels.length]
          });
        });

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'osc_files/lib/mc_images/m'});
      }
      var locations = [
        {lat: -100, lng: 100}
        <?php
					
					while($row = mysqli_fetch_array($data)){
						echo ",";
						echo "{lat:";
						echo str_replace(":", ", lng:", $row["location"]);
						echo "}"; 
					}
					mysqli_data_seek($data, 0);
				?>
      ]
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCZG1fupV2YFC9o9TtDb-vy6luIgLrnYg&callback=initMap">
    </script>

      
	</div>
		<br>
		<p><a class="btn btn-add" href="site-performance.php?table=yeahboi">View Raw Data</a></p>
	<?php if($_GET["table"]){ ?>
	<div class="list">
		<table>
			<tr><th>IP</th><th>Query</th><th>Referer URL</th><th>Browser Type</th><th>Remote Host</th><th>Requested Page</th><th>Hostname / Location</th><th>Region</th><th>Location Lat:Long</th><th>Postcode</th><th>Visited</th></tr>
			<?php

				if(mysqli_num_rows($data) > 0){

			    	while($row = mysqli_fetch_array($data)){
			    		?>
			    		<tr>
			    			<td><?php echo $row['ip'] ?></td>
			    			<td><?php echo $row['query_string'] ?></td>
			    			<td><?php echo $row['referer'] ?></td>
			    			<td><?php echo $row['user_agent'] ?></td>
			    			<td><?php echo $row['remote_host'] ?></td>
			    			<td><?php echo $row['request_uri'] ?></td>
			    			<td><?php echo $row['hostname'] ?></td>
			    			<td><?php echo $row['region'] ?></td>
			    			<td><?php echo $row['location'] ?></td>
			    			<td><?php echo $row['postcode'] ?></td>
			    			<td><?php echo $row['created_at'] ?></td>
			    		</tr>

			    		<?php
			   		}

			   		mysqli_data_seek($data, 0);
			   	}
			   	else{
			    	echo "<tr><td><p>No Visits Found In Range: <strong>" . $date_from . "</strong> to <strong>" . $date_to . "</strong>.</p></td></tr>";
				}
			?>
		</table>
	</div>

	<?php } ?>

	
	


	

</section>


<?php LoadFooter(); ?>
