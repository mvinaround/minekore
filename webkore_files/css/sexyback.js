var ColourArray = ["&4","&6","&2","&3","&1","&5",    "&c","&e","&a","&b","&9","&d"]
var Dark = 0;
var Light = 6;


function SeqDark(){
	col = ColourArray[Dark]
	Dark +=1
	if(Dark == 6){
		Dark = 0;
	}
	return col;
}

function SeqLight(){
	col = ColourArray[Light]
	Light +=1
	if(Light == 12){
		Light = 6;
	}
	return col;
}

function SeqRand(){
	col = ColourArray[Math.floor(Math.random() * 11)]
	return col;
}


function RainbowTextLight(text){
	var string = ""
	for (var i = 0; i < text.length; i++) {
    	string += SeqLight();
    	string += text.charAt(i);
	}
	$("#rbl-text").html(string.replaceColorCodes())
	$("#rbl-output").val(string)

}

$("#gen-text").click(function(){
	RainbowTextLight($("#input").val())
});

function RainbowTextDark(text){
	var string = ""
	for (var i = 0; i < text.length; i++) {
    	string += SeqDark();
    	string += text.charAt(i);
	}
	$("#rbd-text").html(string.replaceColorCodes())
	$("#rbd-output").val(string)
}

function RainbowTextMix(text){
	var string = ""
	for (var i = 0; i < text.length; i++) {
    	string += SeqRand();
    	string += text.charAt(i);
	}
	$("#rbm-text").html(string.replaceColorCodes())
	$("#rbm-output").val(string)
}

$("#gen-text").click(function(){
	RainbowTextLight($("#input").val())
	RainbowTextDark($("#input").val())
	RainbowTextMix($("#input").val())
});













var obfuscators = [];
var styleMap = {
    '&4': 'color:#be0000; text-shadow: 2px 2px 0px #2A0000;',
    '&c': 'color:#fe3f3f; text-shadow: 2px 2px 0px #3F1515;',
    '&6': 'color:#d9a334; text-shadow: 2px 2px 0px #2A2A00;',
    '&e': 'color:#fefe3f; text-shadow: 2px 2px 0px #3F3F15;',
    '&2': 'color:#00be00; text-shadow: 2px 2px 0px #002A00;',
    '&a': 'color:#3ffe3f; text-shadow: 2px 2px 0px #153F15;',
    '&b': 'color:#3ffefe; text-shadow: 2px 2px 0px #153F3F;',
    '&3': 'color:#00bebe; text-shadow: 2px 2px 0px #002A2A;',
    '&1': 'color:#0000be; text-shadow: 2px 2px 0px #00002A;',
    '&9': 'color:#3f3ffe; text-shadow: 2px 2px 0px #15153F;',
    '&d': 'color:#fe3ffe; text-shadow: 2px 2px 0px #3F153F;',
    '&5': 'color:#be00be; text-shadow: 2px 2px 0px #2A002A;',
    '&f': 'color:#ffffff; text-shadow: 2px 2px 0px #3F3F3F;',
    '&7': 'color:#bebebe; text-shadow: 2px 2px 0px #2A2A2A;',
    '&8': 'color:#3f3f3f; text-shadow: 2px 2px 0px #151515;',
    '&0': 'color:#000000; ',
    '&l': 'font-weight:bold',
    '&n': 'text-decoration:underline;text-decoration-skip:spaces',
    '&o': 'font-style:italic',
    '&m': 'text-decoration:line-through;text-decoration-skip:spaces',
};
function obfuscate(string, elem) {
    var magicSpan,
        currNode,
        len = elem.childNodes.length;
    if(string.indexOf('<br>') > -1) {
        elem.innerHTML = string;
        for(var j = 0; j < len; j++) {
            currNode = elem.childNodes[j];
            if(currNode.nodeType === 3) {
                magicSpan = document.createElement('span');
                magicSpan.innerHTML = currNode.nodeValue;
                elem.replaceChild(magicSpan, currNode);
                init(magicSpan);
            }
        }
    } else {
        init(elem, string);
    }
    function init(el, str) {
        var i = 0,
            obsStr = str || el.innerHTML,
            len = obsStr.length;
        obfuscators.push( window.setInterval(function () {
            if(i >= len) i = 0;
            obsStr = replaceRand(obsStr, i);
            el.innerHTML = obsStr;
            i++;
        }, 0) );
    }
    function randInt(min, max) {
        return Math.floor( Math.random() * (max - min + 1) ) + min;
    }
    function replaceRand(string, i) {
        var randChar = String.fromCharCode( randInt(64,90) ); /*Numbers: 48-57 Al:64-90*/
        return string.substr(0, i) + randChar + string.substr(i + 1, string.length);
    }
}
function applyCode(string, codes) {
    var len = codes.length;
    var elem = document.createElement('span'),
        obfuscated = false;
    for(var i = 0; i < len; i++) {
        elem.style.cssText += styleMap[codes[i]] + ';';
        if(codes[i] === '§k') {
            obfuscate(string, elem);
            obfuscated = true;
        }
    }
    if(!obfuscated) elem.innerHTML = string;
    return elem;
}
function parseStyle(string) {
    var codes = string.match(/&.{1}/g) || [],
        indexes = [],
        apply = [],
        tmpStr,
        indexDelta,
        noCode,
        final = document.createDocumentFragment(),
        len = codes.length,
        string = string.replace(/\n|\\n/g, '<br>');
    
    for(var i = 0; i < len; i++) {
        indexes.push( string.indexOf(codes[i]) );
        string = string.replace(codes[i], '\x00\x00');
    }
    if(indexes[0] !== 0) {
        final.appendChild( applyCode( string.substring(0, indexes[0]), [] ) );
    }
    for(var i = 0; i < len; i++) {
    	indexDelta = indexes[i + 1] - indexes[i];
        if(indexDelta === 2) {
            while(indexDelta === 2) {
                apply.push ( codes[i] );
                i++;
                indexDelta = indexes[i + 1] - indexes[i];
            }
            apply.push ( codes[i] );
        } else {
            apply.push( codes[i] );
        }
        if( apply.lastIndexOf('&r') > -1) {
            apply = apply.slice( apply.lastIndexOf('&r') + 1 );
        }
        tmpStr = string.substring( indexes[i], indexes[i + 1] );
        final.appendChild( applyCode(tmpStr, apply) );
    }
    return final;
}
function clearObfuscators() {
    var i = obfuscators.length;
    for(;i--;) {
        clearInterval(obfuscators[i]);
    }
    obfuscators = [];
}
String.prototype.replaceColorCodes = function() {
  clearObfuscators();
  var outputString = parseStyle(String(this));
  return outputString;
};

/////////////////////////////////////////////////
function cutString(str, cutStart, cutEnd){
  return str.substr(0,cutStart) + str.substr(cutEnd+1);
}