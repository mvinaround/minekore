<?php

include("osc_core.php");

function GetSnippet($by_name){
	$snippet = mysqli_fetch_array(SQLQuery("SELECT content FROM snippets WHERE name = '" . $by_name . "' LIMIT 1"));

	if($snippet["content"] == ""){
		return "<strong>Snippet '" . $by_name . "' Not Found.</strong>";
	}
	else{
		return ParseSocialMedia($snippet["content"]);
	}
}

function GetContent($by_name){
	$content = mysqli_fetch_array(SQLQuery("SELECT content FROM contents WHERE name = '" . $by_name . "' LIMIT 1"));

	if($content["content"] == ""){
		return "<strong>Content '" . $by_name . "' Not Found.</strong>";
	}
	else{
		return ParseSocialMedia($content["content"]);
	}
}

function GetAdvSetting($by_name){
	$snippet = mysqli_fetch_array(SQLQuery("SELECT value FROM settings WHERE name = '" . $by_name . "' LIMIT 1"));

	if($snippet["value"] == ""){
		return "";
	}
	else{
		return $snippet["value"];
	}
}

function GetContactDetails(){

	if(strlen(GetAdvSetting('landline')) > 0){
		?>
			<h3>Landline: <a href="tel:<?php echo GetAdvSetting('landline') ?>"><?php echo GetAdvSetting('landline') ?></a></h3>
		<?php
	} 

	if(strlen(GetAdvSetting('mobile')) > 0){
		?>
			<h3>Mobile: <a href="tel:<?php echo GetAdvSetting('mobile') ?>"><?php echo GetAdvSetting('mobile') ?></a></h3>
		<?php
	} 

	if(strlen(GetAdvSetting('email')) > 0){
		?>
			<h3>Email: <a href="mailto:<?php echo GetAdvSetting('email') ?>?Subject=Enquiry"><?php echo GetAdvSetting('email') ?></a></h3>
		<?php
	}
}


function GetTestimonials(){
	$result = SQLQuery("SELECT * FROM testimonials");

	if(mysqli_num_rows($result) > 0){

		while($row = mysqli_fetch_array($result)){
		?>
		<h3 class="testimonials"><?php echo htmlspecialchars($row["title"]) ?></h3>
		<p><?php echo htmlspecialchars($row["content"]) ?></p>
		<p class="author"><?php echo htmlspecialchars($row["from_client"]) ?></p>
		<br><br>


		<?php
		}
	}
}


function GetMenu(){
	$result = SQLQuery("SELECT * FROM menu_items WHERE visible = 1");
	$link_html_template = GetSetting("menu_link_line");


	if(mysqli_num_rows($result) > 0){

		while($row = mysqli_fetch_array($result)){
			echo ReplaceContents($link_html_template,"{LINK}",HTMLLinkGenerator($row["name"],$row["link"]));
		}
	}
}


function GetServices(){
	$result = SQLQuery("SELECT * FROM services ");

	if(mysqli_num_rows($result) > 0){

		while($row = mysqli_fetch_array($result)){
		?>

		<div class="service">
			<div class="image-container">
				<div class="image-ratio" style="background-image: url('<?php echo $row["image"] ?>')"></div>
			</div>
			<div class="service-text">
				<h3><?php echo $row["title"] ?></h3>
				<p><?php echo $row["description"] ?></p>
			</div>

		</div>

		<?php
		}
	}
}




function GetGallery(){
			$files = glob('admin/gallery/*.{jpg,png,}', GLOB_BRACE);
			foreach($files as $file) {
					$image_id = rand(1,12345)
				?>
				<div class="image" onclick="ImageClicked(<?php echo $image_id ?>)" data-image-url="<?php echo $file ?>" id="img-<?php echo $image_id ?>" style="height: 293px; background-image: url('<?php echo $file ?>');"></div>

				<?php
			}


	
}

function GetBanners(){
			$files = glob('admin/scrollers/*.{jpg,png,}', GLOB_BRACE);
			$image_id = 1;
			foreach($files as $file) {

				?>
				<div class="scr" id="scr-<?php echo $image_id ?>" style="background-image: url('<?php echo $file ?>');"></div>
				<?php

				$image_id += 1;

			}

			echo "<script> var TotalImages = " . ($image_id - 1) . "</script>";
	
}

//Social Media Tie Ins -------------------------------------

function GetFacebookSDK(){
	if(GetFeature(1,0,"social-media")){
		?>
		<script>
		$(document).ready(function() {
  			$.ajaxSetup({ cache: true });
  			$.getScript('https://connect.facebook.net/en_US/sdk.js', function(){
				FB.init({
      				appId: '181687705935449',
     				version: 'v2.7' // or v2.1, v2.2, v2.3, ...
    			});     
    			$('#loginbutton,#feedbutton').removeAttr('disabled');
    		FB.getLoginStatus(updateStatusCallback);
  			});
		});
		</script>

		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.12&appId=181687705935449&autoLogAppEvents=1';
  			fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>

		<?php
	}
}

function GetFacebookMessageBox(){
	if(GetSettingBool(1,0,"fb_enable_message_box_on_contact_page")){
		?>
			<div class="fb-page" data-href="<?php echo GetSetting("facebook_url") ?>" data-tabs="messages" data-small-header="false" data-adapt-container-width="true" data-hide-cover="<?php echo GetSettingBool("true","false", "fb_direct_message_hide_cover") ?>" data-show-facepile="true"></div>
		<?php
	}
}