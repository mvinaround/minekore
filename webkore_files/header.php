<?php
 $webkore_version = "2.1.5";
?>

<html>
<head>
	<title>WebKore CMS - <?php echo $webkore_version; ?></title>
	<link rel="stylesheet" href="webkore_files/css/webkore.css" />
	<meta name="theme-color" content="#086ccd">
	<link rel="stylesheet" href="webkore_files/css/font-awesome.min.css">
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<script src="webkore_files/osc_js_core.js"></script>
	<script src="webkore_files/lib/bootstrap-tagsinput.js"></script>
	<script src="webkore_files/lib/js.cookie.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript" src="webkore_files/lib/moment-with-locales.js"></script>
	

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body id="WebKore">