/*
 __          __  _     _  __                __  __                  
 \ \        / / | |   | |/ /               |  \/  |                 
  \ \  /\  / /__| |__ | ' / ___  _ __ ___  | \  / | ___ _ __  _   _ 
   \ \/  \/ / _ \ '_ \|  < / _ \| '__/ _ \ | |\/| |/ _ \ '_ \| | | |
    \  /\  /  __/ |_) | . \ (_) | | |  __/ | |  | |  __/ | | | |_| |
     \/  \/ \___|_.__/|_|\_\___/|_|  \___| |_|  |_|\___|_| |_|\__,_|
                                                                    
                                                                    
===========================================================================================================================

Created By: Jamie Overington
Copyright: 2016
Created 01/02/2016
Last Updated: 24/02/2016

===[ Requirements ]========================================
 - Requires JQuery 2+ (Whole script)
 - Requires osc_js_core.js (For calendar use)
===========================================================================================================================
*/

$(document).ready(function(){
	$(".toggle").click(function(d){
		ToggleClick(d)
	})

})


function ToggleClick(d){
	var MenuGroup = d["currentTarget"]["id"].replace("-toggle","")
	var Icon = d["currentTarget"]["id"]

	var IconElement = $("#" + Icon)
	var selectedEmement = $("#" + MenuGroup)

		if(selectedEmement.attr("data-hidden") == "true"){
			//Open
			selectedEmement.attr("data-hidden", "false").removeClass("collapsed").removeClass('no-animation')
			IconElement.removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-up")

			Cookies.set(MenuGroup,"open")
		}
		else if(selectedEmement.attr("data-hidden") == "false"){
			//Closed
			selectedEmement.attr("data-hidden", "true").addClass("collapsed")
			IconElement.removeClass("fa-chevron-circle-up").addClass("fa-chevron-circle-down")
			Cookies.set(MenuGroup,"closed")



		}
}

function ProcessCookies(){
	$(".toggle").each(function(){
		element = $(this).attr('id').replace("-toggle","")

		state = Cookies.get(element)

		//console.log("Element: " + element)
		//console.log("State: " + state )


		if(state == "closed"){
			$("#" + element).attr("data-hidden","true").addClass("collapsed").addClass('no-animation')
			$("#" + element + "-toggle").removeClass("fa-chevron-circle-up").addClass("fa-chevron-circle-down")
		}


	})

}

$(document).ready(function() {
	ProcessCookies();
});