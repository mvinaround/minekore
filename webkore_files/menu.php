<?php CheckForUser(); ?>
<ul class="menubar mobile-menubar-closed">
	<li class="osc-logo-mb" onclick="ToggleMobileMenu()"><i class="fa fa-bars" aria-hidden="true"></i></li>
	<li><a href="dashboard.php"><i class="fa fa-dashboard"></i>Dashboard</a></li>

	<li class="section-title">Website Management<i id="website_management-toggle" class="toggle fa fa-chevron-circle-up"></i></li>
	<div class="collapsible" id="website_management" data-hidden="false">

		<li><a href="content-dashboard.php"><i class="fa fa-file-text"></i>Content</a></li>
		<li><a href="advanced.php"><i class="fa fa-cogs"></i>Advanced</a></li>
		

		<?php if(GetFeature(1,0,"site-performance")){ ?>
			<li><a href="site-performance.php"><i class="fa fa-line-chart"></i>Performance</a></li>
		<?php } ?>

		<?php if(GetFeature(1,0,"social-media")){ ?>
			<li><a href="social-media.php"><i class="fa fa-share-square"></i>Social Media</a></li>
		<?php } ?>
	</div>

	<li class="section-title">Minecraft Management<i id="mc-toggle" class="toggle fa fa-chevron-circle-up"></i></li>
	<div class="collapsible" id="mc" data-hidden="false">
		<li><a href="#"><i class="fa fa-"></i>Server Settings</a></li>
		<li><a href="#"><i class="fa fa-"></i>Players</a></li>
	</div>

	<li class="section-title">System Management<i id="sys-toggle" class="toggle fa fa-chevron-circle-up"></i></li>
	<div class="collapsible" id="sys" data-hidden="false">
		<li><a href="users.php"><i class="fa fa-users"></i>Users</a></li>
		<li><a href="logs-dashboard.php"><i class="fa fa-eye"></i>System Logs</a></li>
	</div>

	<?php if(IsAdmin(1,0)){ ?>
	<li class="section-title">Superuser<i id="su-toggle" class="toggle fa fa-chevron-circle-up"></i></li>
	<div class="collapsible" id="su" data-hidden="false">
		<li><a href="superuser.php"><i class="fa fa-user-secret"></i>Superuser!</a></li>
	</div>
	<?php } ?>

	<li class="section-title"></li>
	<li class="mb-bottom"><a href="index.php?action=logout"><i class="fa fa-power-off"></i>Log Out</a></li>
</ul>

<script src="webkore_files/osc_js_menu.js"></script>