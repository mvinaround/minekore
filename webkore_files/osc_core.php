<?php
//System settings ----------------------------------------------------------------------
//System In Development Mode (Shows errors and useful info):
$all_errors = true;
require("connection/wk_connect.php");
//Enable Sessions?
session_start();

//Site CSS (To incorperate into editor)
$site_css_link = "";

//Global variables (can be useful for somethings)
$current_user = "";
$key = "jamieoverington";
$enc_key = base64_encode($key);
$password_salt = "9d7e3a4f06e2b976a494caf6725b6bbf9b47cf3f";

//Required Stuff:

require("connection/wk_connect.php"); // PATH TO CONNECTION FILE!
ini_set('display_errors', 1);		
$connection = mysqli_connect($db_server_ip, $db_user, $db_password,$db_database);
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}		

//--------------------------------------------------------------------------------------

/* BLACKLISTED EMAIL ADDRESSES: 
	None.
   ----------------------------
 */


//---------------------- MAILGUN CONNECTION SETTINGS: ----------------------------------
include("lib/mailgun/al.php");

function SendEmail($from_email,$from_name,$to_email,$to_name,$reply_address,$email_body, $email_subject){

	$email_sent = false;

	$mail = new PHPMailer;

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'smtp.mailgun.org';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'mail@mg.jjmediauk.co.uk';                 // SMTP username
	$mail->Password = 'jamieo';                           // SMTP password
	$mail->SMTPSecure = 'tcp';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                    // TCP port to connect to

	$mail->setFrom($from_email,$from_name);
	$mail->addAddress($to_email, $to_name);
	$mail->addReplyTo($reply_address, 'Information');
	//gtrdisco@hotmail.com
	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Body = $email_body;
	$mail->Subject = $email_subject;

	if($mail->send()){
		$email_sent =  true;
	}

	SQLQuery("INSERT INTO emails (from_email,from_name,to_email,to_name,reply_address,email_body,email_subject,sent) VALUES ('" . SQLSafe($from_email) . "','" . SQLSafe($from_name) . "','" . SQLSafe($to_email) . "','" . SQLSafe($to_name) . "','" . SQLSafe($reply_address) . "','" . SQLSafe($email_body) . "','" . SQLSafe($email_subject) . "','" . SQLSafe($email_sent) . "')");

	return $email_sent;

}

function emailProcessQueue($template, $client_array){


}




function GenerateResetPasswordEmail($email){
	LogAction("Password Reset Emailed to: " . $email);

	if(mysqli_num_rows(SQLQuery("SELECT * FROM users WHERE email = '" . SQLSafe($email) . "' LIMIT 1")) == 1){	
		$key = RandomString(12);
		$site = GetSetting("main_site_url");
		SQLQuery("INSERT INTO password_reset_key (email,reset_key) VALUES('" . $email ."','" . $key . "')");

		$reset_email = '<h1>WebKore CMS - Password Reset Key:</h1><br><p>Reset Link: <a href="http://' . $site . '/admin/forgot-password.php?view=change-password&key=' . $key . '">http://' . $site . '/admin/forgot-password.php?view=change-password&key=' . $key . '</a></p>';


		SendEmail("webkore@jjmediauk.co.uk","WebKore CMS",$email,"","no-reply@jjmediauk.co.uk",$reset_email,"WebKore CMS - Password Reset Email");
	}

}

function ResetPassword($email,$password,$key){
	//Error Tree: E1 = Invalid Key, E2 = Invalid Email
	LogAction("Password Reset on Account: " . $email);
	$query = SQLQuery("SELECT * FROM password_reset_key WHERE reset_key = '" . SQLSafe($key) . "'");

	if(mysqli_num_rows($query) == 1){
		$data = mysqli_fetch_array($query);
		if($email == $data["email"]){
			if(SQLQuery("UPDATE users SET password = '" . HashPassword($password) . "' WHERE email = '" . $email . "'")){
				
				SQLQuery("DELETE FROM password_reset_key WHERE reset_key ='" . $key . "'");
				return 1;
			}

		}
		else{
			return "E2";
		}
	}
	else{
		return "E1";
	}

}

//--------------------------------------------------------------------------------------

//Disable Errors?
//MASTER:
$ErrorsAreNoMatchForMe = 0;
//


if($ErrorsAreNoMatchForMe == 0){
	if($all_errors){
		error_reporting( error_reporting() & ~E_NOTICE );
	}
	else{
		error_reporting(E_ERROR);
	}
	
}
else{
	error_reporting(0);
}






function SQLSafe($value){
	global $connection;
	return(mysqli_real_escape_string($connection, stripslashes($value)));
}

function SQLWrap($value){
	global $connection;
	$value = mysqli_real_escape_string($connection, $value);
	return $value ;
}


function SQLQuery($query){
	global $connection;
	$result = mysqli_query($connection, $query);
	if (!$result && $in_dev) {
    	printf("Error: %s\n", mysqli_error($connection));
    exit();
}
	return $result;

}

//used to change form field checkbox -> database boolean.
function SQLCheckbox($val){
	if($val == "on"){
		return "1";
	}
	else{
		return "0";
	}
}

function HashPassword($pass){
	$hash_browns = password_hash($pass,PASSWORD_DEFAULT);
	return $hash_browns;
}

function CheckPassword($username,$pass){
	$data = mysqli_fetch_array(SQLQuery("SELECT * FROM users WHERE email = '" . SQLSafe($username) . "' AND enabled = 1"));
	$password = $data["password"];
	if(password_verify($pass,$password) == 1){
		return 1;
	}
	else{
		return 0;
	}
}

//used to change database boolean -> form field checkbox checked property.
function FormCheckbox($item){
	$e = "";
	if($item == 1 ){
		$e = "checked";
	}

	return $e;
}

function HTMLLinkGenerator($name,$addr){
	return "<a href='" . $addr . "'>" . $name . "</a>";
}

function ReplaceContents($contents,$replace_this,$with_this){
	return str_replace($replace_this,$with_this,$contents);
}

function FormField($name,$type,$required,$value,$placeholder){
	$safe_name = str_replace(" ","_", strtolower($name));
	$placeholder_written = false;

	if($value == "get-setting"){
		$value = GetSetting($safe_name);
	}

	echo '<div class="form-field">';
	echo 	'<label for="' . $safe_name .'">' . $name;
	if($value){
		if($placeholder){
			echo " - " . $placeholder;
			$placeholder_written = true;
		}
	}
	if($type == "checkbox" or $type == "date"){
		if($placeholder_written == false){
			echo " - " . $placeholder;
		}
	}
	echo 	'</label>';
	echo 	'<input type="' . $type . '" name="' . $safe_name . '" id="' . $safe_name . '" placeholder="' . $placeholder .'" ';
	if($type == "checkbox" and $value == "1" or $value == "on"){
		echo " checked ";
	}
	if($type != "checkbox"){
		echo '" value="' . $value . '" ';
	}
	if($required){
		echo "required";
	} 
	echo '/>';
	echo '</div>' . "\n";
}

function FormTextarea($name,$required,$value){
	$safe_name = str_replace(" ","_", strtolower($name));

	echo '<div class="form-field">';
	echo 	'<label for="' . $safe_name .'">' . $name;
	if($value){
		if($placeholder){
			echo " - " . $placeholder;
		}
	}
	echo 	'</label>';
	echo 	'<textarea name="' . $safe_name .'" ';
	if($required){
		echo "required";
	} 
	echo '>';
	echo $value;
	echo '</textarea></div>';
}

function UpdateSetting($setting_id){

	if(mysqli_num_rows(SQLQuery("SELECT * FROM settings WHERE name ='" . $setting_id . "'")) > 0 ){

		if(SQLQuery("UPDATE settings SET value = '" . SQLWrap($_POST[$setting_id]) . "' WHERE name = '" . $setting_id . "' ")){
			return 1;
		}
		else{
			return 0;
		}
	}
	else{
		if(SQLQuery("INSERT INTO settings (name,value) VALUES ('" . $setting_id . "','" . SQLWrap($_POST[$setting_id]) . "')")){
			return 1;
		}
		else{
			return 0;
		}
	}	
}

function GenerateNewDesktopKey(){

		$key = GenerateSerial();

	if(mysqli_num_rows(SQLQuery("SELECT * FROM settings WHERE name ='desktop_software_key'")) > 0 ){

		if(SQLQuery("UPDATE settings SET value = '" . SQLWrap($key) . "' WHERE name = 'desktop_software_key' ")){
			return $key;
		}
		else{
			return 0;
		}
	}
	else{
		if(SQLQuery("INSERT INTO settings (name,value) VALUES ('desktop_software_key','" . SQLWrap($key) . "')")){
			return $key;
		}
		else{
			return 0;
		}
	}

}

function GetSetting($setting_name){
	$return =  mysqli_fetch_array(SQLQuery("SELECT value FROM settings WHERE name ='" . $setting_name . "'"));

	return $return["value"];
}

function GetSettingBool($on,$off,$setting_name){
	$feature =  mysqli_fetch_array(SQLQuery("SELECT value FROM settings WHERE name ='" . $setting_name . "'"));

	if($feature["value"] == "checkbox" and $feature["value"] == "1" or $feature["value"] == "on"){
		return $on;
	}
	else{
		return $off;
	}
}

function GetVisitorData(){
	$data = SQLQuery("SELECT * FROM visits WHERE bot = 0");
}

function GetFeature($on,$off,$feature){
	//returns on or off depending on what the database has in its boolean
	$data = mysqli_fetch_array(SQLQuery("SELECT enabled FROM features WHERE name = '" . $feature . "' LIMIT 1"));

	if($data[0] == 1){
		return $on;
	}
	else{
		return $off;
	}
}


function MakeUKDate($date){
	return date("d-m-Y", strtotime($date));
}

function Get24hTime($date){
	return date("g:i A", strtotime($date));
}


function Paid($bool){
	if($bool == "1"){
		return "Paid";
	}
	else{
		return "Not Paid";
	}
}

function TickCross($val){
	if($val == 1){
		echo '<i class="fa fa-check tc-tick"></i>';
	}
	else{
		echo '<i class="fa fa-times tc-cross"></i>';	
	}
}




// Login & Auth --------------------------------------------------------------------------------

function CheckLogin($email, $password){
	if(CheckPassword($email,$password)){
		$_SESSION['user'] = $email;

		session_write_close();
		header("location:dashboard.php");

		LogAction("User Logged In");
	}
	else{

		$ip = $_SERVER['REMOTE_ADDR'];
		$query_string = $_SERVER['QUERY_STRING'];
		$http_referer = $_SERVER['HTTP_REFERER'];
		$http_user_agent = $_SERVER['HTTP_USER_AGENT'];
		$remote_host = $_SERVER['REMOTE_HOST'];
		$request_uri = $_SERVER['REQUEST_URI'];

		SQLQuery("INSERT INTO login_attempts (ip,query,referer,email,password) VALUES ('" . SQLWrap($ip) . "','" . SQLWrap($query_string) . "','" . SQLWrap($http_referer) . "','" . SQLWrap($email) . "','" . SQLWrap($password) . "')");

		return 0;
	}
	
}

function CheckForUser(){
	$login_query = SQLQuery("SELECT * FROM users WHERE email = '" . $_SESSION['user'] . "' AND enabled = 1");

	if(mysqli_num_rows($login_query) == 1){	
		$data = mysqli_fetch_array($login_query);
		$current_user = $data["name"];
	}
	else{
		header("location:index.php");
		SQLQuery("INSERT INTO user_logs (username,ip,action) VALUES ('" . SQLWrap("No User") . "','" . SQLWrap($_SERVER['REMOTE_ADDR']) . "','" . SQLWrap("user not logged in.<br>QUERY:" . $_SERVER['QUERY_STRING']) . "') ");
		mysqli_close($connection);
		die('<div class="alert danger">Hack Detected.</div>');
	}
	
}

function GetUsername(){
	$query = SQLQuery("SELECT name FROM users WHERE email = '" . $_SESSION['user'] . "'");

	if(mysqli_num_rows($query) == 1){	
		$data = mysqli_fetch_array($query);
		$current_user = $data["name"];
	}
	return $current_user;
}

function LogAction($log_this){
	$ip = $_SERVER['REMOTE_ADDR'];
	$user = GetUsername();
	SQLQuery("INSERT INTO user_logs (username,ip,action) VALUES ('" . SQLWrap($user) . "','" . SQLWrap($ip) . "','" . SQLWrap($log_this) . "') ");
}

function CheckForAdmin(){
	$login_query = SQLQuery("SELECT * FROM users WHERE email = '" . $_SESSION['user'] . "' AND admin = 1 AND enabled = 1");

	if(mysqli_num_rows($login_query) == 1){	
		$current_user = $_SESSION["admin"];
	}
	else{
		header("location:dashboard.php");
	}
	
}

function IsAdmin($yes, $no){
	$login_query = SQLQuery("SELECT * FROM users WHERE email = '" . $_SESSION['user'] . "' AND admin = 1 AND enabled = 1");

	if(mysqli_num_rows($login_query) == 1){	
		return $yes;
	}
	else{
		return $no;
	}
	
}

function CheckForUserLoginScreen(){
	$login_query = SQLQuery("SELECT * FROM users WHERE email = '" . $_SESSION['user'] . "' AND password = '" . $_SESSION['password'] . "' AND enabled = 1");


	if(mysqli_num_rows($login_query) == 1){	
		header("location:dashboard.php");
	}
}

function RandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function GenerateSerial(){
	$string = RandomString(6) . "-" . RandomString(6) . "-" . RandomString(6) . "-" . RandomString(6);
	return $string;
}




// System Visuals------------------------------------------------------------------------
$system_files_directory = "webkore_files/";

function FriendlyBool($bool){
	if($bool == "1"){
		return '<i class="data-green fa fa-check"></i>';
	}
	else{
		return '<i class="data-red fa fa-times"></i>';
	}
}

function LoadHeader(){
	
	include($system_files_directory . "header.php");
}

function LoadFooter(){
	include($system_files_directory . "footer.php");
}

function LoadMenu(){
	include($system_files_directory . "menu.php");
}

function m_LoadHeader(){
	include($system_files_directory . "header.php");
}

function m_LoadFooter(){
	include($system_files_directory . "footer.php");
}

function m_LoadMenu(){
	include($system_files_directory . "menu.php");
}


function MakeEditor($byid,$size){
	echo '<div class="form-field"><br><a class="btn btn-info" onclick="ImageManager();">Manage Images (Popup window)</a><br><br></div>';

	echo "<script>
	tinymce.init({
		selector:'" . $byid . "',
		height:" . $size . ",
		content_css : '" . $site_css_link . "' ,
		content_css: [''],
		plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak','searchreplace wordcount visualblocks visualchars code fullscreen','insertdatetime media nonbreaking save table contextmenu directionality','emoticons template paste textcolor colorpicker textpattern imagetools'],
  		toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  		toolbar2: 'print preview media | forecolor backcolor emoticons',
  		image_advtab: true,
  		document_base_url : 'admin/'

	})</script>";
}




function StoreVisitorData(){
	// Getting the information
	
	$ip = $_SERVER['REMOTE_ADDR'];
	$query_string = $_SERVER['QUERY_STRING'];
	$http_referer = $_SERVER['HTTP_REFERER'];
	$http_user_agent = $_SERVER['HTTP_USER_AGENT'];
	$remote_host = $_SERVER['REMOTE_HOST'];
	$request_uri = $_SERVER['REQUEST_URI'];

	// Check for a bot:

	$is_a_bot = "0";

	$botlist = array("Teoma", "alexa", "froogle", "Gigabot", "inktomi","looksmart", "URL_Spider_SQL", "Firefly", "NationalDirectory","Ask Jeeves", "TECNOSEEK", "InfoSeek", "WebFindBot", "girafabot","crawler", "www.galaxy.com", "Googlebot", "Scooter", "Slurp","msnbot", "appie", "FAST", "WebBug", "Spade", "ZyBorg", "rabaz","Baiduspider", "Feedfetcher-Google", "TechnoratiSnoop", "Rankivabot","Mediapartners-Google", "Sogou web spider", "WebAlta Crawler","TweetmemeBot","Butterfly","Twitturls","Me.dium","Twiceler","Purebot","facebookexternalhit","Yandex","CatchBot","W3C_Validator","Jigsaw","PostRank","Purebot","Twitterbot","Voyager","zelist",);

	foreach($botlist as $bot){
		if(strpos($_SERVER['HTTP_USER_AGENT'],$bot)!==false)
		$is_a_bot = $bot;
	}

	//Check Browser Type:

	if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE){
		$browser_type = 'Internet explorer (Old)';
	}
	if(strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') !== FALSE){
		$browser_type =  "Safari";
	}
	if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== FALSE){
		$browser_type = 'Google Chrome';
	}
	if(strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== FALSE){ //For Supporting IE 11
		$browser_type = 'Internet explorer';
	}
	if(strpos($_SERVER['HTTP_USER_AGENT'], 'Edge') !== FALSE){ //For Supporting IE 11
		$browser_type = 'Microsoft Edge';
	}
	if(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== FALSE){
		$browser_type = 'Mozilla Firefox';
	}
	if(strpos($_SERVER['HTTP_USER_AGENT'], 'Opera') !== FALSE){
		$browser_type = "Opera";
	}
	if(strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== FALSE){
		$browser_type = "Opera Mini";
	}
	if($browser_type == ""){
		$browser_type = 'Unknown';
	}

	//Get Location and hostname etc...

	$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
	$hostname = $details->city;
	$region = $details->region;
	$location = $details->loc;
	$location =  str_replace(",", ":", $location);
	$postcode = $details->postal;

	//Write to DB

	SQLQuery("INSERT INTO visits (ip,query_string,referer,user_agent,remote_host,request_uri,bot,hostname,region,location,postcode) VALUES ('". SQLWrap($ip) . "','" . SQLWrap($query_string) . "','" . SQLWrap($referer) ."','" . SQLWrap($browser_type) ."','" . SQLWrap($remote_host) ."','" . SQLWrap($request_uri) ."','" . SQLWrap($is_a_bot) ."','" . SQLWrap($hostname) ."','" . SQLWrap($region) ."','" . SQLWrap($location) ."','" . SQLWrap($postcode) ."')");
}


function SwitchFeature($feature_name){
	echo '<div class="onoffswitch">';
	echo '<input type="checkbox"  class="onoffswitch-checkbox" id="f_' . $feature_name . '" ' . GetFeature("checked","",$feature_name) . '>';
	echo '<label class="onoffswitch-label" for="f_' . $feature_name . '"></label>';
	echo '</div>';
}


// Social Media -------------------------------------------------------------------------------------------------------------

function ConstructFacebookLikeButton($page){
	$string = "\n" . '<div class="fb-like" data-href="' . $page . '" data-layout="' . GetSetting("fb_like_layout") . '" data-action="' . GetSetting("fb_like_action_type") . '" data-size="' . GetSetting("fb_like_button_size") . '" data-colorscheme="' . GetSetting("fb_like_button_colour") . '" data-show-faces="true" data-share="true"></div>' . "\n" ;

		//$string = $string . "<strong>LIKE URL:" . $page . "</strong>";

	return $string;

}

function ParseSocialMedia($cont){
	$done = $cont;
	if(GetFeature(1,0,"social-media")){
		$done = ReplaceContents($cont,"{FB_LIKE}",ConstructFacebookLikeButton(GetSetting("facebook_url")));
	}
	else{
		$done = ReplaceContents($cont,"{FB_LIKE}","");
	}

	return $done; 

}