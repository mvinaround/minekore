<?php include("webkore_files/osc_core.php");
	LoadHeader();
	CheckForUserLoginScreen();

	if($_POST["action"] == "reset-send"){
		if(empty($_POST["email"]) == 0){
			GenerateResetPasswordEmail($_POST["email"]);	
		}
	}

	if($_POST["action"] == "reset-password"){
		ResetPassword($_POST["email"],$_POST["password"],$_POST["key"]);
	}
	
?>
<div class="osc-logo"></div>
<div class="login-screen">
<?php if($_GET["view"] == "reset"){ ?>

	<h2>Forgotten Password</h2>
	<p>Please enter your email address below. If there is an associated account, you will receive a reset link in your inbox.</p>
	<br>
	<form method="post" action="forgot-password.php?view=email-sent">
		<div class="form-field">
			<label for="email">Email Address:</label>
			<input type="email" name="email" value="<?php echo $email; ?>" requried>
		</div>
		<div class="form-field">
			<input type="submit" value="Reset Password" requried>
			<input type="hidden" name="action" value="reset-send">
		</div>
	</form>


	<?php
	}
	elseif($_GET["view"] == "change-password"){
	 ?>
	<h2>Set Your New Password</h2>
	<p>Please enter your email address below along with your new password.</p>
	<br>
	 <form method="post" action="forgot-password.php">
		<div class="form-field">
			<label for="email">Email Address:</label>
			<input type="email" name="email" requried>
		</div>
		<div class="form-field">
			<label for="password">New Password:</label>
			<input type="password" name="password" requried>
		</div>
		<div class="form-field">
			<input type="submit" value="Reset Password" requried>
			<input type="hidden" name="action" value="reset-password">
			<input type="hidden" name="key" value="<?php echo $_GET['key'] ?>">
		</div>
	</form>


	 <?php }
	 elseif($_GET["view"] == "email-sent"){ ?>
	 <h2>Email Sent</h2>

	 <?php
	 }
	 else{
	 ?>
	 <h2>All Done! <a href="index.php">Login Here</a></h2>
	 <?php
	 }
	 ?>
	<br>
	<p>Created &amp; Maintained By: <br>Jamie Overington &amp; JJMedia UK &copy; 2017 - <?php echo date("Y"); ?></p>
</div>



