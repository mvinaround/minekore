<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	
	$alert_box = false;
	$alert_text = "";
	$alert_type = "";


	if(isset($_FILES['image'])){
      $errors= array();
      $file_name = $_FILES['image']['name'];
      $file_size =$_FILES['image']['size'];
      $file_tmp =$_FILES['image']['tmp_name'];
      $file_type=$_FILES['image']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
      
      $expensions= array("jpeg","jpg","png");
      
      if(in_array($file_ext,$expensions)=== false){
         $errors[]=" Please choose a JPEG or PNG file.";
         $alert_text = $alert_text . " Please choose a JPEG or PNG file.";
      }
      
      if($file_size > 2097152){
         $errors[]=' The image needs to be less than 2MB in size.';
         $alert_text = $alert_text . ' The image needs to be less than 2MB in size.';
      }
      
      if(empty($errors)==true){
         move_uploaded_file($file_tmp,"scrollers/".$file_name);
         LogAction("Uploaded a new banner [" . $file_name . "] ");

      }else{
         $alert_box = true;
         $alert_type = "danger";
      }
   }

?>
<section>
	<h1>Site Banners</h1>
	<p>Upload new site banners here. These are shown at the top of your site usually. To delete a banner, simply click on it (You will have to confirm it).</p>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>
	<div class="images">
		<h2>Site Banners</h2>
		<?php
			$files = glob('scrollers/*.{jpg,png,}', GLOB_BRACE);
			foreach($files as $file) {
					$image_id = rand(1,12345)
				?>

				<div class="banner-image" id="image-<?php echo $image_id ?>" data-fs-url="<?php echo $file ?>" style="background-image: url('<?php echo $file ?>')">
					<a onclick="ImageDelete(<?php echo $image_id; ?>)">Delete</a>
				</div>

				<?php
			}

		?>
	</div>

	<div class="add-image">
	<h2>Upload A New Banner</h2>
		<form action="" method="POST" enctype="multipart/form-data">
		<div class="form-field">
			<label for="file">Select An Image</label>
			<input type="file" name="image"  accept="image/*"/>
			<input type="submit" value="Upload"/>
		</div>

      </form>
	</div>


</section>
<?php LoadFooter(); ?>
